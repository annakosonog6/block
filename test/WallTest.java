import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;

public class WallTest {
//    public static void main(String[] args) {
//        Predicate<String> dluzszyNiz5 = s -> s.length() > 5;
//        test(dluzszyNiz5);
//    }
//
//    public static void test(Predicate<String> predicate) {
//        System.out.print(predicate.test("HEJ"));
//    }

    private final static Block BLOCK1 = new Block("red", "cement");
    private final static Block BLOCK2 = new Block("blue", "cement2");

    private static final CompositeBlock COMPOSITE_BLOCK_BLOCK3 = new CompositeBlock("black", "cement3");
    private final static Block BLOCK4 = new Block("blue", "cement4");

    private static final CompositeBlock COMPOSITE_BLOCK_BLOCK5 = new CompositeBlock("pink", "cement5");
    private final static Block BLOCK6 = new Block("yellow", "cement6");


    private Wall emptyWall;
    private Wall filledWall;


    @BeforeAll
    static void setBlocks() {
        COMPOSITE_BLOCK_BLOCK3.addNewBlock(BLOCK4);
        COMPOSITE_BLOCK_BLOCK3.addNewBlock(COMPOSITE_BLOCK_BLOCK5);

        COMPOSITE_BLOCK_BLOCK5.addNewBlock(BLOCK6);
    }

    @BeforeEach
    void setUp() {
        emptyWall = new Wall();
        filledWall = new Wall(Arrays.asList(BLOCK1, BLOCK2, COMPOSITE_BLOCK_BLOCK3));

    }


    @Test
    void shouldReturnAnElementWhenBlockContainsTheGivenColor() {
        //assertEquals("expected", opt.get());
        assertEquals("red", filledWall.findBlockByColor("red").get().getColor());
    }

    @Test
    void shouldNotReturnWhenBlockNotContainsTheGivenColor() {
        //     assertEquals("", filledWall.findBlockByColor("red").orElse(UnexpectedException::new));
    }

    @Test
    void shouldReturnAnElementWhenBlockContainsTheGivenMaterial() {
        assertThat(filledWall.findBlocksByMaterial("cement2"), is(BLOCK2));
    }

    @Test
    void shouldReturnNullWhenNotFoundByMaterial() {
        assertThat(filledWall.findBlocksByMaterial("xxx"), is(nullValue()));
    }

    @Test
    void shouldHandleCountProperlyWithNestedStructure() {
        assertThat(filledWall.count(), is(6));
    }
}
