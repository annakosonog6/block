import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class Wall implements IStructure {

    private List<IBlock> blocks;

    public Wall(List<IBlock> blocks) {
        this.blocks = blocks;
    }

    public Wall() {
    }

    @Override
    public Optional<IBlock> findBlockByColor(String color) {
        return blocks.stream()
                .flatMap(IBlock::toStream)
                .filter(blocks -> blocks.getColor().contains(color))
                .findFirst();
    }

    @Override
    public List<IBlock> findBlocksByMaterial(String material) {
        if (material == null) {
            throw new IllegalArgumentException("Material is null!");
        }
        return blocks.stream()
                .flatMap(IBlock::toStream)
                .filter(blocksMaterial -> blocksMaterial.getMaterial().equals(material))
                .collect(Collectors.toList());
    }

    @Override
    public int count() {
        return (int) blocks.stream()
                .flatMap(IBlock::toStream)
                .count();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Wall)) return false;
        Wall wall = (Wall) o;
        return Objects.equals(blocks, wall.blocks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(blocks);
    }
}
