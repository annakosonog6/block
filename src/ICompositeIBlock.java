import java.util.List;

interface ICompositeIBlock extends IBlock {
    List<IBlock> getBlocks();
}
