import java.util.Objects;
import java.util.stream.Stream;

public class Block implements IBlock {

    private final String color;
    private final String material;


    public Block(String color, String material) {
        this.color = color;
        this.material = material;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getMaterial() {
        return material;
    }

    @Override
    public Stream<IBlock> toStream() {
        return Stream.of(this);
    }

    @Override
    public String toString() {
        return "Block: " +
                "color: " + color +
                ", material: " + material;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Block block = (Block) o;
        return Objects.equals(color, block.color) &&
                Objects.equals(material, block.material);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, material);
    }
}
